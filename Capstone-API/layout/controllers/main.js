const BASE_URL = "https://62de511fccdf9f7ec2d50a65.mockapi.io";
var listItem = [];
var cart = [];

function getDSSP() {
  openLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      listItem = res.data.map((sp) => {
        return new SanPham(
          sp.id,
          sp.name,
          sp.price,
          sp.screen,
          sp.backCamera,
          sp.frontCamera,
          sp.img,
          sp.desc,
          sp.type
        );
      });
      renderListItem(listItem);
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}
getDSSP();

function addItemToCart(id) {
  let index = listItem.findIndex((item) => {
    return item.id == id;
  });

  let indexItemInCart = cart.findIndex((item) => {
    return item.id == id;
  });

  if (indexItemInCart == -1) {
    cart.push({ ...listItem[index], soLuong: 1 });
  } else {
    cart[indexItemInCart].soLuong++;
  }

  renderCartItem(cart);
}

function removeItemInCart(id) {
  let index = cart.findIndex((item) => {
    return item.id == id;
  });
  cart.splice(index, 1);
  renderCartItem(cart);
}

function clearCart() {
  cart = [];
  renderCartItem(cart);
}

function tangSoLuong(id) {
  let indexItemInCart = cart.findIndex((item) => {
    return item.id == id;
  });
  cart[indexItemInCart].soLuong++;
  renderCartItem(cart);
}

function giamSoLuong(id) {
  let indexItemInCart = cart.findIndex((item) => {
    return item.id == id;
  });
  cart[indexItemInCart].soLuong--;
  renderCartItem(cart);
}
