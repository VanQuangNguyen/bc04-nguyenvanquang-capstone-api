function renderListItem(dssp) {
  var contentHTML = "";

  dssp.forEach((sp) => {
    var contentDiv = `
  <div class="card">
    <div class="top-bar">
      <i class="fab fa-${sp.type}"></i>
      <em class="stocks">In Stock</em>
    </div>
    <div class="img-container">
      <img class="product-img" src="${sp.img}" alt="" />
      <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
    </div>
    <div class="details">
      <div class="name-fav">
        <strong class="product-name text-primary">${sp.name}</strong>
        <button onclick="" class="heart"><i class="fas fa-heart"></i></button>
      </div>
      <div class="wrapper">
        <h5>${sp.desc}</h5>
        <p>${sp.backCamera}; ${sp.frontCamera}; ${sp.screen}</p>
      </div>
      <div class="purchase">
        <p class="product-price text-danger">${sp.price}$</p>
        <span class="btn-add">
          <div>
            <button onclick="addItemToCart(${sp.id})" class="add-btn">
              Add <i class="fas fa-chevron-right"></i>
            </button></div
        ></span>
      </div>
    </div>
  </div>
    `;
    contentHTML += contentDiv;
  });
  document.getElementById("listItem").innerHTML = contentHTML;
}

function renderCartItem(cartItems) {
  var contentHTML = "";

  cartItems.forEach((cartItem) => {
    var tongTienMotSanPham = cartItem.price * cartItem.soLuong;
    var contentDiv = `
  <div class="cart-item">
    <div class="cart-img">
      <img src="${cartItem.img}" alt="">
    </div>
    <strong class="name">${cartItem.name}</strong>
    <span class="qty-change">
  <div>
    <button class="btn-qty" onclick="giamSoLuong(${cartItem.id})"><i class="fas fa-chevron-left"></i></button>
    <p class="qty">${cartItem.soLuong}</p>
    <button class="btn-qty" onclick="tangSoLuong(${cartItem.id})"><i class="fas fa-chevron-right"></i></button>
  </div></span>
    <p class="price">${tongTienMotSanPham}$</p>
    <button onclick="removeItemInCart(${cartItem.id})"><i class="fas fa-trash"></i></button>
  </div>
    `;
    contentHTML += contentDiv;
  });
  document.getElementById("cart-items").innerHTML = contentHTML;
}

function btnOpenCart() {
  document.getElementById("coverCart").style.display = "block";
  document.getElementById("cart").style.right = "0px";
}

function btnCloseCart() {
  document.getElementById("coverCart").style.display = "none";
  document.getElementById("cart").style.right = "-100%";
}

function openLoading() {
  document.getElementById("loading").style.display = "flex";
}

function closeLoading() {
  document.getElementById("loading").style.display = "none";
}
