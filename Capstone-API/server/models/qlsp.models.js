function SanPhamWithId(id, name, price, img, desc) {
  this.id = id;
  this.name = name;
  this.price = price;
  this.img = img;
  this.desc = desc;
}

function SanPhamWithoutId(name, price, img, desc) {
  this.name = name;
  this.price = price;
  this.img = img;
  this.desc = desc;
}
