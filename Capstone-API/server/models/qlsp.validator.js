var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return true;
    }
  },

  kiemTraLaSo: function (value, idError, message) {
    const re = /[0-9]/i;

    if (re.test(value)) {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return false;
    }
  },
};
