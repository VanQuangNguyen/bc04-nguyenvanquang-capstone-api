function layThongTinTuForm() {
  const name = document.getElementById("TenSP").value;
  const price = document.getElementById("GiaSP").value;
  const img = document.getElementById("HinhSP").value;
  const desc = document.getElementById("MoTa").value;

  return new SanPhamWithoutId(name, price, img, desc);
}

function renderDSSP(dssp) {
  var contentHTML = "";

  dssp.forEach((sp) => {
    var contentTr = `<tr>
    <td>${sp.id}</td>
    <td>${sp.name}</td>
    <td>${sp.price}</td>
    <td><img style=" width: 100px;" src="${sp.img}"></td>
    <td>${sp.desc}</td>
    <td>
    <button onclick="xoaSanPham('${sp.id}')" class="btn btn-danger"><i class="fa fa-trash"></i></button>
    <button  data-toggle="modal"
    data-target="#myModal" onclick="suaSanPham('${sp.id}')" class="btn btn-warning"><i class="fa fa-edit"></i></button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}

function formValidation(sp) {
  // kiem tra ten san pham
  var isValid = validator.kiemTraRong(
    sp.name,
    "tbTSP",
    "Tên sản phẩm không được để trống."
  );

  // kiem tra gia san pham
  isValid =
    isValid &
    (validator.kiemTraRong(
      sp.price,
      "tbGSP",
      "Giá sản phẩm không được để trống."
    ) &&
      validator.kiemTraLaSo(
        sp.price,
        "tbGSP",
        "Giá sản phẩm nhập vào phải là số."
      ));
  //kiem tra hinh anh
  isValid =
    isValid &
    validator.kiemTraRong(
      sp.img,
      "tbHSP",
      "Hình sản phẩm không được để trống."
    );

  // kiem tra mo ta san pham
  isValid =
    isValid &
    validator.kiemTraRong(
      sp.desc,
      "tbMT",
      "Mô tả sản phẩm không được để trống."
    );
  return isValid;
}

function hideMessageValidator() {
  var messageElementIds = ["tbTSP", "tbGSP", "tbHSP", "tbMT"];

  messageElementIds.forEach((messageElementId) => {
    document
      .getElementById(messageElementId)
      .classList.replace("sp-show-thongbao", "sp-thongbao");
  });
}

function resetData() {
  document.getElementById("TenSP").value = "";
  document.getElementById("GiaSP").value = "";
  document.getElementById("HinhSP").value = "";
  document.getElementById("MoTa").value = "";
}

function openLoading() {
  document.getElementById("loading").style.display = "flex";
}

function closeLoading() {
  document.getElementById("loading").style.display = "none";
}

function btnThemSP() {
  hideMessageValidator();
  document.getElementById("TenSP").value = "";
  document.getElementById("GiaSP").value = "";
  document.getElementById("HinhSP").value = "";
  document.getElementById("MoTa").value = "";
  document.getElementById(
    "modal-footer"
  ).innerHTML = `<div class="text-right mb-2">
  <button onclick="themSP()" class="btn btn-success" id="btnAdd">Add Products</button>
</div>`;
}
