const BASE_URL = "https://62de511fccdf9f7ec2d50a65.mockapi.io";
var listSp = [];
function getDSSP() {
  openLoading();
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then(function (res) {
      closeLoading();
      console.log(res);

      listSp = res.data.map(
        (sp) => new SanPhamWithId(sp.id, sp.name, sp.price, sp.img, sp.desc)
      );
      renderDSSP(listSp);
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}
getDSSP();

function themSP() {
  var newSP = layThongTinTuForm();

  var isValid = formValidation(newSP);

  if (isValid == false) {
    return;
  } else {
    openLoading();
    axios({
      url: `${BASE_URL}/products`,
      method: "POST",
      data: newSP,
    })
      .then(function (res) {
        closeLoading();
        console.log(res);
        getDSSP();
        resetData();
      })
      .catch(function (err) {
        closeLoading();
        console.log(err);
      });
  }
}

function xoaSanPham(id) {
  openLoading();
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      getDSSP();
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}

var idSP = "";
function suaSanPham(id) {
  hideMessageValidator();
  openLoading();
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "GET",
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      idSP = res.data.id;
      document.getElementById("TenSP").value = res.data.name;
      document.getElementById("GiaSP").value = res.data.price;
      document.getElementById("HinhSP").value = res.data.img;
      document.getElementById("MoTa").value = res.data.desc;
      document.getElementById(
        "modal-footer"
      ).innerHTML = `<div class="text-right mb-2">
      <button onclick="capNhatSanPham()" class="btn btn-success">Update Products</button>
    </div>`;
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}

function capNhatSanPham() {
  var newSP = layThongTinTuForm();

  var isValid = formValidation(newSP);

  if (isValid == false) {
    return;
  } else {
    openLoading();
    axios({
      url: `${BASE_URL}/products/${idSP}`,
      method: "PUT",
      data: newSP,
    })
      .then(function (res) {
        closeLoading();
        console.log(res);
        getDSSP();
        resetData();
      })
      .catch(function (err) {
        closeLoading();
        console.log(err);
      });
  }
}

function timSanPham() {
  var searchKey = document.getElementById("inputTK").value;

  var dsspFiltered = listSp.filter((sp) => {
    return sp.name.toLowerCase().includes(searchKey.toLowerCase());
  });
  renderDSSP(dsspFiltered);
}
